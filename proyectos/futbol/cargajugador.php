<?php
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Jugador</title>
 
	<!-- Bootstrap -->
	

<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="dropzone.js"></script>
<script src="jquery-3.6.0.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
	
	
	<link href="bootstrap.min.css" rel="stylesheet">
	<link href="bootstrap-datepicker.css" rel="stylesheet">
	<link href="style_nav.css" rel="stylesheet">
	
	<style>
		.content {
			margin-top: 80px;
		}
	</style>
 
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	
	<div class="container">
	
		<div class="content">
			<h2>Registrar jugador &raquo; </h2>
			<hr />
 
			<?php
			if(isset($_POST['add'])){
				 
				$NombreApellido		     = mysqli_real_escape_string($con,(strip_tags($_POST["NombreApellido"],ENT_QUOTES)));
				$idUsuario		     = mysqli_real_escape_string($con,(strip_tags($_POST["DNI"],ENT_QUOTES)));
				$Clave	 = mysqli_real_escape_string($con,(strip_tags($_POST["Clave"],ENT_QUOTES)));
				$RepetirClave	 = mysqli_real_escape_string($con,(strip_tags($_POST["RepetirClave"],ENT_QUOTES)));
				$Ciudad	 = mysqli_real_escape_string($con,(strip_tags($_POST["Ciudad"],ENT_QUOTES)));
				$Provincia	     = mysqli_real_escape_string($con,(strip_tags($_POST["Provincia"],ENT_QUOTES)));
				$Calle		 = mysqli_real_escape_string($con,(strip_tags($_POST["Calle"],ENT_QUOTES)));
				$Numerocalle		 = mysqli_real_escape_string($con,(strip_tags($_POST["Numerocalle"],ENT_QUOTES)));
				$Piso			 = mysqli_real_escape_string($con,(strip_tags($_POST["Piso"],ENT_QUOTES)));
				$Barrio		     = mysqli_real_escape_string($con,(strip_tags($_POST["Barrio"],ENT_QUOTES)));
				$codigoPostal		     = mysqli_real_escape_string($con,(strip_tags($_POST["codigoPostal"],ENT_QUOTES)));
				$FechaNacimiento	 = mysqli_real_escape_string($con,(strip_tags($_POST["FechaNacimiento"],ENT_QUOTES)));
				$Nacionalidad	 = mysqli_real_escape_string($con,(strip_tags($_POST["Nacionalidad"],ENT_QUOTES)));
				$Sexo	     = mysqli_real_escape_string($con,(strip_tags($_POST["Sexo"],ENT_QUOTES)));
				$TipoDocumento		 = mysqli_real_escape_string($con,(strip_tags($_POST["TipoDocumento"],ENT_QUOTES)));
				$NumeroDocumento		 = mysqli_real_escape_string($con,(strip_tags($_POST["NumeroDocumento"],ENT_QUOTES)));
				$Alturajugador			 = mysqli_real_escape_string($con,(strip_tags($_POST["Alturajugador"],ENT_QUOTES)));
				$Posicion			 = mysqli_real_escape_string($con,(strip_tags($_POST["idPosicion"],ENT_QUOTES)));
				$Peso		     = mysqli_real_escape_string($con,(strip_tags($_POST["Peso"],ENT_QUOTES)));
				$Piernahabil		     = mysqli_real_escape_string($con,(strip_tags($_POST["Piernahabil"],ENT_QUOTES)));
				$Fichado	 = mysqli_real_escape_string($con,(strip_tags($_POST["Fichado"],ENT_QUOTES)));
				$clubFichado	 = mysqli_real_escape_string($con,(strip_tags($_POST["clubFichado"],ENT_QUOTES)));
				$descripcion	 = mysqli_real_escape_string($con,(strip_tags($_POST["Descripcion"],ENT_QUOTES)));
			
				
				$idVideo		 = mysqli_real_escape_string($con,(strip_tags($_POST["fileToUpload"],ENT_QUOTES)));
				$AceptoT		 = mysqli_real_escape_string($con,(strip_tags($_POST["AceptoT"],ENT_QUOTES)));
				
				
				$NombreApellidoT		 = mysqli_real_escape_string($con,(strip_tags($_POST["NombreApellidoT"],ENT_QUOTES)));
				$CiudadT		 = mysqli_real_escape_string($con,(strip_tags($_POST["CiudadT"],ENT_QUOTES)));
				$ProvinciaT		 = mysqli_real_escape_string($con,(strip_tags($_POST["ProvinciaT"],ENT_QUOTES)));
				$CalleT		 = mysqli_real_escape_string($con,(strip_tags($_POST["CalleT"],ENT_QUOTES)));
				$NumeroT		 = mysqli_real_escape_string($con,(strip_tags($_POST["NumerocalleT"],ENT_QUOTES)));
				$PisoT		 = mysqli_real_escape_string($con,(strip_tags($_POST["PisoT"],ENT_QUOTES)));
				$departamentoT		 = mysqli_real_escape_string($con,(strip_tags($_POST["DepartamentoT"],ENT_QUOTES)));
				$BarrioT		 = mysqli_real_escape_string($con,(strip_tags($_POST["BarrioT"],ENT_QUOTES)));
				$codigoPostalT		 = mysqli_real_escape_string($con,(strip_tags($_POST["CodigoPostalT"],ENT_QUOTES)));
				$MailT		 = mysqli_real_escape_string($con,(strip_tags($_POST["MailT"],ENT_QUOTES)));
				$VigenciaDesdeT		 = mysqli_real_escape_string($con,(strip_tags($_POST["VigenciaDesdeT"],ENT_QUOTES)));
				$VigenciaHastaT		 = mysqli_real_escape_string($con,(strip_tags($_POST["VigenciaHastaT"],ENT_QUOTES)));
				$NacionalidadT		 = mysqli_real_escape_string($con,(strip_tags($_POST["NacionalidadT"],ENT_QUOTES)));
				$TipoDocumentoT		 = mysqli_real_escape_string($con,(strip_tags($_POST["TipoDocumentoT"],ENT_QUOTES)));
				$NumerodocumentoT		 = mysqli_real_escape_string($con,(strip_tags($_POST["NumerodocumentoT"],ENT_QUOTES)));
				$CelularT		 = mysqli_real_escape_string($con,(strip_tags($_POST["CelularT"],ENT_QUOTES)));
				
			
			    $Posicionx =  SUBSTR($Posicion,0,0);
echo $Posicionx;


			
				
			
			
			
 echo "llegue -1";
 
 
 
              
 
 
                $insert = mysqli_query($con, "INSERT INTO `tutor`(`IdTutor`, `NombreApellido`, `Ciudad`, `Provincia`, `Calle`, `Numero`, `Piso`, `departamento`, `Barrio`, 
				 `codigoPostal`, `Mail`, `VigenciaDesde`, `VigenciaHasta`, `Nacionalidad`, `TipoDocumento`, `Numerodocumento`, `Celular`) VALUES 
				 ('[value-1]','$NombreApellidoT','$CiudadT','$ProvinciaT','$CalleT', '$NumeroT','$PisoT','$departamentoT','$BarrioT','$codigoPostalT',
				 '$MailT','$VigenciaDesdeT','$VigenciaHastaT','$NacionalidadT','$TipoDocumentoT','$NumerodocumentoT','$CelularT')") or die(mysqli_error());
				
echo "llegue 0";
                 $idtutor = $con->insert_id;																	      
																		   
		echo "llegue 1";				
 
          if ($Clave <> $RepetirClave) {
			   echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. Claves no coinciden</div>';
			  }else{
	
				$cek = mysqli_query($con, "SELECT * FROM jugador WHERE idjugador='1'");
				
				if(mysqli_num_rows($cek) == 0){
						$insert = mysqli_query($con, "INSERT INTO `jugador`(`IdJugador`, `NombreApellido`, `idUsuario`, `Clave`, `RepetirClave`,`Ciudad`, `Provincia`, `Calle`, `Numerocalle`, `Piso`, `Barrio`, `codigoPostal`, `FechaNacimiento`, `Nacionalidad`, `Sexo`, `TipoDocumento`, `NumeroDocumento`, `Alturajugador`, `idPosicion`,`Peso`, `Piernahabil`, `Fichado`, `clubFichado`, `Descripcion`, `idTutor`, `idVideo`, `Acepto`) VALUES 
						                                                   ('[value-1]','$NombreApellido','$idUsuario','$Clave'  ,'$RepetirClave','$Ciudad','$Provincia','$Calle','$Numerocalle','$Piso','$Barrio','$codigoPostal','$FechaNacimiento','$Nacionalidad','$Sexo','$TipoDocumento','$NumeroDocumento','$Alturajugador','$Posicion','$Peso','$Piernahabil','$Fichado','$clubFichado','$descripcion','$idtutor','$idVideo','$AceptoT')") or die("Problemas al insertar".mysqli_error($con));
						if($insert){
							
							echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Bien hecho! Los datos han sido guardados con éxito.</div>';
							// <li><a href="Busquedajugador.php" class="nav-link px-2 link-dark"><strong> Ingresar</strong></a></li>
						}else{
							echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. No se pudo guardar los datos !</div>';
						}
					 
				}else{
					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. código exite!</div>';
				}
	
				}
			}

			?>
			
			
			
 
			<form class="form-horizontal" action="" method="post">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nombre y apelido</label>
					<div class="col-sm-3">
						<input type="text" name="NombreApellido" class="form-control" placeholder="Nombres" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">DNI</label>
					<div class="col-sm-3">
						<input type="text" name="DNI" class="form-control" placeholder="DNI" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Clave</label>
					<div class="col-sm-3">
						<input type="text" name="Clave" class="form-control" placeholder="clave" required>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Repetir clave</label>
					<div class="col-sm-3">
						<input type="text" name="RepetirClave" class="form-control" placeholder="repetir clave" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Ciudad</label>
					<div class="col-sm-3">
					    <input type="text" name="Ciudad" class="form-control" placeholder="Ciudad required>
					
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Provincia</label>
					<div class="col-sm-3">
					<select name="Provincia">
					     <option value="Buenos Aires">Bs. As.</option>
          <option value="Catamarca">Catamarca</option>
          <option value="Chaco">Chaco</option>
          <option value="Chubut">Chubut</option>
          <option value="Cordoba">Cordoba</option>
          <option value="Corrientes">Corrientes</option>
          <option value="Entre Rios">Entre Rios</option>
          <option value="Formosa">Formosa</option>
          <option value="Jujuy">Jujuy</option>
          <option value="La Pampa">La Pampa</option>
          <option value="La Rioja">La Rioja</option>
          <option value="Mendoza">Mendoza</option>
          <option value="Misiones">Misiones</option>
          <option value="Neuquen">Neuquen</option>
          <option value="Rio Negro">Rio Negro</option>
          <option value="Salta">Salta</option>
          <option value="San Juan">San Juan</option>
          <option value="San Luis">San Luis</option>
          <option value="Santa Cruz">Santa Cruz</option>
          <option value="Santa Fe">Santa Fe</option>
          <option value="Sgo. del Estero">Sgo. del Estero</option>
          <option value="Tierra del Fuego">Tierra del Fuego</option>
         <option value="Tucuman">Tucuman</option>
		 </select>
					
		
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Calle</label>
					<div class="col-sm-3">
						<input type="text" name="Calle" class="form-control" placeholder="Calle" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Numero Calle</label>
					<div class="col-sm-3">
						<input type="text" name="Numerocalle" class="form-control" placeholder="Numerocalle" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Piso</label>
					<div class="col-sm-3">
						<input type="text" name="Piso" class="form-control" placeholder="Piso" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Barrio</label>
					<div class="col-sm-3">
						<input type="text" name="Barrio" class="form-control" placeholder="Barrio" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Codigo Postal</label>
					<div class="col-sm-3">
						<input type="text" name="codigoPostal" class="form-control" placeholder="codigoPostal" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Fecha nacimiento</label>
					<div class="col-sm-3">
						<input type="date" name="FechaNacimiento" class="form-control" placeholder="FechaNacimiento" required>
					</div>
				</div>
				<td class="cr25"><span class="e07nss">Nacionalidad</span></td>
<td class="cr25"> <br> <select name="Nacionalidad"class="drp" style="width:250;height:20" width=250>
   
    <option value="argentina">Argentina</option>
    <option value="20">Afganistán</option>
    <option value="30">Albania</option>
    <option value="40">Alemania</option>
    <option value="50">Andorra</option>
    <option value="60">Angola</option>
    <option value="70">Anguilla</option>
    <option value="80">Antártida Argentina</option>
    <option value="90">Antigua y Barbuda</option>
    <option value="100">Antillas Holandesas</option>
    <option value="110">Arabia Saudita</option>
    <option value="120">Argelia</option>
    <option value="130">Armenia</option>
    <option value="140">Aruba</option>
    <option value="150">Australia</option>
    <option value="160">Austria</option>
    <option value="170">Azerbaiján</option>
    <option value="180">Bahamas</option>
    <option value="190">Bahrain</option>
    <option value="200">Bangladesh</option>
    <option value="210">Barbados</option>
    <option value="220">Bélgica</option>
    <option value="230">Belice</option>
    <option value="240">Benin</option>
    <option value="250">Bhutan</option>
    <option value="260">Bielorusia</option>
    <option value="bolivia">Bolivia</option>
    <option value="280">Bosnia Herzegovina</option>
    <option value="290">Botswana</option>
    <option value="brasil">Brasil</option>
    <option value="310">Brunei</option>
    <option value="320">Bulgaria</option>
    <option value="330">Burkina Faso</option>
    <option value="340">Burundi</option>
    <option value="350">Cabo Verde</option>
    <option value="360">Camboya</option>
    <option value="370">Camerún</option>
    <option value="380">Canadá</option>
    <option value="390">Chad</option>
    <option value="chile">Chile</option>
    <option value="410">China</option>
    <option value="420">Chipre</option>
    <option value="430">Colombia</option>
    <option value="440">Comoros</option>
    <option value="450">Congo</option>
    <option value="460">Corea del Norte</option>
    <option value="470">Corea del Sur</option>
    <option value="480">Costa de Marfil</option>
    <option value="490">Costa Rica</option>
    <option value="500">Croacia</option>
    <option value="510">Cuba</option>
    <option value="520">Darussalam</option>
    <option value="530">Dinamarca</option>
    <option value="540">Djibouti</option>
    <option value="550">Dominica</option>
    <option value="560">Ecuador</option>
    <option value="570">Egipto</option>
    <option value="580">El Salvador</option>
    <option value="590">Em. Arabes Un.</option>
    <option value="600">Eritrea</option>
    <option value="610">Eslovaquia</option>
    <option value="620">Eslovenia</option>
    <option value="espana">España</option>
    <option value="640">Estados Unidos</option>
    <option value="650">Estonia</option>
    <option value="660">Etiopía</option>
    <option value="670">Fiji</option>
    <option value="680">Filipinas</option>
    <option value="690">Finlandia</option>
    <option value="700">Francia</option>
    <option value="710">Gabón</option>
    <option value="720">Gambia</option>
    <option value="730">Georgia</option>
    <option value="740">Ghana</option>
    <option value="750">Gibraltar</option>
    <option value="760">Grecia</option>
    <option value="770">Grenada</option>
    <option value="780">Groenlandia</option>
    <option value="790">Guadalupe</option>
    <option value="800">Guam</option>
    <option value="810">Guatemala</option>
    <option value="820">Guayana Francesa</option>
    <option value="830">Guinea</option>
    <option value="840">Guinea Ecuatorial</option>
    <option value="850">Guinea-Bissau</option>
    <option value="860">Guyana</option>
    <option value="870">Haití</option>
    <option value="880">Holanda</option>
    <option value="890">Honduras</option>
    <option value="900">Hong Kong</option>
    <option value="910">Hungría</option>
    <option value="920">India</option>
    <option value="930">Indonesia</option>
    <option value="940">Irak</option>
    <option value="950">Irán</option>
    <option value="960">Irlanda</option>
    <option value="970">Islandia</option>
    <option value="980">Islas Cayman</option>
    <option value="990">Islas Cook</option>
    <option value="1000">Islas Faroe</option>
    <option value="1010">Islas Marianas del Norte</option>
    <option value="1020">Islas Marshall</option>
    <option value="1030">Islas Solomon</option>
    <option value="1040">Islas Turcas y Caicos</option>
    <option value="1050">Islas Vírgenes</option>
    <option value="1060">Islas Wallis y Futuna</option>
    <option value="1070">Israel</option>
    <option value="1080">Italia</option>
    <option value="1090">Jamaica</option>
    <option value="1100">Japón</option>
    <option value="1110">Jordania</option>
    <option value="1120">Kazajstán</option>
    <option value="1130">Kenya</option>
    <option value="1140">Kirguistán</option>
    <option value="1150">Kiribati</option>
    <option value="1160">Kuwait</option>
    <option value="1170">Laos</option>
    <option value="1180">Lesotho</option>
    <option value="1190">Letonia</option>
    <option value="1200">Líbano</option>
    <option value="1210">Liberia</option>
    <option value="1220">Libia</option>
    <option value="1230">Liechtenstein</option>
    <option value="1240">Lituania</option>
    <option value="1250">Luxemburgo</option>
    <option value="1260">Macao</option>
    <option value="1270">Macedonia</option>
    <option value="1280">Madagascar</option>
    <option value="1290">Malasia</option>
    <option value="1300">Malawi</option>
    <option value="1310">Mali</option>
    <option value="1320">Malta</option>
    <option value="1330">Marruecos</option>
    <option value="1340">Martinica</option>
    <option value="1350">Mauricio</option>
    <option value="1360">Mauritania</option>
    <option value="1370">Mayotte</option>
    <option value="1380">México</option>
    <option value="1390">Micronesia</option>
    <option value="1400">Moldova</option>
    <option value="1410">Mónaco</option>
    <option value="1420">Mongolia</option>
    <option value="1430">Montserrat</option>
    <option value="1440">Mozambique</option>
    <option value="1450">Myanmar</option>
    <option value="1460">Namibia</option>
    <option value="1470">Nauru</option>
    <option value="1480">Nepal</option>
    <option value="1490">Nicaragua</option>
    <option value="1500">Níger</option>
    <option value="1510">Nigeria</option>
    <option value="1520">Noruega</option>
    <option value="1530">Nueva Caledonia</option>
    <option value="1540">Nueva Zelandia</option>
    <option value="1550">Omán</option>
    <option value="1570">Pakistán</option>
    <option value="1580">Panamá</option>
    <option value="1590">Papua Nueva Guinea</option>
    <option value="paraguay">Paraguay</option>
    <option value="1610">Perú</option>
    <option value="1620">Pitcairn</option>
    <option value="1630">Polinesia Francesa</option>
    <option value="1640">Polonia</option>
    <option value="1650">Portugal</option>
    <option value="1660">Puerto Rico</option>
    <option value="1670">Qatar</option>
    <option value="1680">RD Congo</option>
    <option value="1690">Reino Unido</option>
    <option value="1700">República Centroafricana</option>
    <option value="1710">República Checa</option>
    <option value="1720">República Dominicana</option>
    <option value="1730">Reunión</option>
    <option value="1740">Rumania</option>
    <option value="1750">Rusia</option>
    <option value="1760">Rwanda</option>
    <option value="1770">Sahara Occidental</option>
    <option value="1780">Saint Pierre y Miquelon</option>
    <option value="1790">Samoa</option>
    <option value="1800">Samoa Americana</option>
    <option value="1810">San Cristóbal y Nevis</option>
    <option value="1820">San Marino</option>
    <option value="1830">Santa Elena</option>
    <option value="1840">Santa Lucía</option>
    <option value="1850">Sao Tomé y Príncipe</option>
    <option value="1860">Senegal</option>
    <option value="1870">Serbia y Montenegro</option>
    <option value="1880">Seychelles</option>
    <option value="1890">Sierra Leona</option>
    <option value="1900">Singapur</option>
    <option value="1910">Siria</option>
    <option value="1920">Somalia</option>
    <option value="1930">Sri Lanka</option>
    <option value="1940">Sudáfrica</option>
    <option value="1950">Sudán</option>
    <option value="1960">Suecia</option>
    <option value="1970">Suiza</option>
    <option value="1980">Suriname</option>
    <option value="1990">Swazilandia</option>
    <option value="2000">Taiwán</option>
    <option value="uruguay">Uruguay</option>
</select>
</td>
				<div class="form-group">
					<label class="col-sm-3 control-label">Sexo</label>
					<div class="col-sm-3">
					  	 <select name="Sexo">
							<option>M</option>
							<option>F</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Tipo documento</label>
					<div class="col-sm-3">
						<input type="text" name="TipoDocumento" class="form-control" placeholder="TipoDocumento" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Numero documento</label>
					<div class="col-sm-3">
						<input type="text" name="NumeroDocumento" class="form-control" placeholder="NumeroDocumento" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Altura Jugador (ejemplo 1.70 usar punto como separador decimal) </label>
					<div class="col-sm-3">
						<input type="text" name="Alturajugador" class="form-control" placeholder="Alturajugador" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Posicion</label>
					<div class="col-sm-3">
						<select name="idPosicion">
						 <option> 1-Arquero</option>
<option>2-Lateral izquierdo</option>
<option>3-Lateral derecho</option>
<option>4-Central izquierdo</option>
<option>5-Central derecho</option>
<option>6-Medio campista Ofensivo</option>
<option>7-Medio campista Defensivo</option>
<option>8-Interno</option>
<option>9-Centro delantero</option>
<option>10-Segunda Punta</option>
<option>11-Extremo Derecho</option>
<option>12-Extremo izquierdo</option>

						</select>
					</div>
				</div>
				
				
				
				
				 
				<div class="form-group">
					<label class="col-sm-3 control-label">Peso</label>
					<div class="col-sm-3">
						<input type="text" name="Peso" class="form-control" placeholder="Peso" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Pierna Habil</label>
					<div class="col-sm-3">
						<input type="text" name="Piernahabil" class="form-control" placeholder="Piernahabil" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Fichado</label>
					<div class="col-sm-3">
						<input type="text" name="Fichado" class="form-control" placeholder="Fichado" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Club Fichado</label>
					<div class="col-sm-3">
						<input type="text" name="clubFichado" class="form-control" placeholder="clubFichado" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Descripcion</label>
					<div class="col-sm-3">
						<input type="text" name="Descripcion" class="form-control" placeholder="Descripcion" required>
					</div>
				</div>
				<br>
				<h2>Datos del tutor &raquo; </h2>
				
				<br>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nombre y Apellido</label>
					<div class="col-sm-3">
						<input type="text" name="NombreApellidoT" class="form-control" placeholder="NombreApellido" required>
					</div>
				</div>
				
				
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Ciudad</label>
					<div class="col-sm-3">
						<textarea name="CiudadT" class="form-control" placeholder="Ciudad"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Provincia</label>
					<div class="col-sm-3">
					<select name="ProvinciaT">
					     <option value="Buenos Aires">Bs. As.</option>
          <option value="Catamarca">Catamarca</option>
          <option value="Chaco">Chaco</option>
          <option value="Chubut">Chubut</option>
          <option value="Cordoba">Cordoba</option>
          <option value="Corrientes">Corrientes</option>
          <option value="Entre Rios">Entre Rios</option>
          <option value="Formosa">Formosa</option>
          <option value="Jujuy">Jujuy</option>
          <option value="La Pampa">La Pampa</option>
          <option value="La Rioja">La Rioja</option>
          <option value="Mendoza">Mendoza</option>
          <option value="Misiones">Misiones</option>
          <option value="Neuquen">Neuquen</option>
          <option value="Rio Negro">Rio Negro</option>
          <option value="Salta">Salta</option>
          <option value="San Juan">San Juan</option>
          <option value="San Luis">San Luis</option>
          <option value="Santa Cruz">Santa Cruz</option>
          <option value="Santa Fe">Santa Fe</option>
          <option value="Sgo. del Estero">Sgo. del Estero</option>
          <option value="Tierra del Fuego">Tierra del Fuego</option>
         <option value="Tucuman">Tucuman</option>
		 </select>
					
		
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Calle</label>
					<div class="col-sm-3">
						<input type="text" name="CalleT" class="form-control" placeholder="Calle" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Numero Calle</label>
					<div class="col-sm-3">
						<input type="text" name="NumerocalleT" class="form-control" placeholder="Numerocalle" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Piso</label>
					<div class="col-sm-3">
						<input type="text" name="PisoT" class="form-control" placeholder="Piso" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Departamento</label>
					<div class="col-sm-3">
						<input type="text" name="DepartamentoT" class="form-control" placeholder="Departamento" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Barrio</label>
					<div class="col-sm-3">
						<input type="text" name="BarrioT" class="form-control" placeholder="Barrio" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Codigo Postal</label>
					<div class="col-sm-3">
						<input type="text" name="CodigoPostalT" class="form-control" placeholder="codigoPostal" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Mail</label>
					<div class="col-sm-3">
						<input type="text" name="MailT" class="form-control" placeholder="Mail" required>
					</div>
				</div>
					<div class="form-group">
					<label class="col-sm-3 control-label">Comfirmar Mail</label>
					<div class="col-sm-3">
						<input type="text" name="MailT" class="form-control" placeholder="Mail" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">VigenciaDesde</label>
					<div class="col-sm-3">
						<input type="date" name="VigenciaDesdeT" class="form-control" placeholder="VigenciaDesde" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">VigenciaHasta</label>
					<div class="col-sm-3">
						<input type="date" name="VigenciaHastaT" class="form-control" placeholder="VigenciaHasta" required>
					</div>
				</div>
				
			<td class="cr25"><span class="e07nss">Nacionalidad</span></td>
<td class="cr25"> <br> <select name="NacionalidadT"class="drp" style="width:250;height:20" width=250>
   
    <option value="argentina">Argentina</option>
    <option value="20">Afganistán</option>
    <option value="30">Albania</option>
    <option value="40">Alemania</option>
    <option value="50">Andorra</option>
    <option value="60">Angola</option>
    <option value="70">Anguilla</option>
    <option value="80">Antártida Argentina</option>
    <option value="90">Antigua y Barbuda</option>
    <option value="100">Antillas Holandesas</option>
    <option value="110">Arabia Saudita</option>
    <option value="120">Argelia</option>
    <option value="130">Armenia</option>
    <option value="140">Aruba</option>
    <option value="150">Australia</option>
    <option value="160">Austria</option>
    <option value="170">Azerbaiján</option>
    <option value="180">Bahamas</option>
    <option value="190">Bahrain</option>
    <option value="200">Bangladesh</option>
    <option value="210">Barbados</option>
    <option value="220">Bélgica</option>
    <option value="230">Belice</option>
    <option value="240">Benin</option>
    <option value="250">Bhutan</option>
    <option value="260">Bielorusia</option>
    <option value="bolivia">Bolivia</option>
    <option value="280">Bosnia Herzegovina</option>
    <option value="290">Botswana</option>
    <option value="brasil">Brasil</option>
    <option value="310">Brunei</option>
    <option value="320">Bulgaria</option>
    <option value="330">Burkina Faso</option>
    <option value="340">Burundi</option>
    <option value="350">Cabo Verde</option>
    <option value="360">Camboya</option>
    <option value="370">Camerún</option>
    <option value="380">Canadá</option>
    <option value="390">Chad</option>
    <option value="chile">Chile</option>
    <option value="410">China</option>
    <option value="420">Chipre</option>
    <option value="430">Colombia</option>
    <option value="440">Comoros</option>
    <option value="450">Congo</option>
    <option value="460">Corea del Norte</option>
    <option value="470">Corea del Sur</option>
    <option value="480">Costa de Marfil</option>
    <option value="490">Costa Rica</option>
    <option value="500">Croacia</option>
    <option value="510">Cuba</option>
    <option value="520">Darussalam</option>
    <option value="530">Dinamarca</option>
    <option value="540">Djibouti</option>
    <option value="550">Dominica</option>
    <option value="560">Ecuador</option>
    <option value="570">Egipto</option>
    <option value="580">El Salvador</option>
    <option value="590">Em. Arabes Un.</option>
    <option value="600">Eritrea</option>
    <option value="610">Eslovaquia</option>
    <option value="620">Eslovenia</option>
    <option value="espana">España</option>
    <option value="640">Estados Unidos</option>
    <option value="650">Estonia</option>
    <option value="660">Etiopía</option>
    <option value="670">Fiji</option>
    <option value="680">Filipinas</option>
    <option value="690">Finlandia</option>
    <option value="700">Francia</option>
    <option value="710">Gabón</option>
    <option value="720">Gambia</option>
    <option value="730">Georgia</option>
    <option value="740">Ghana</option>
    <option value="750">Gibraltar</option>
    <option value="760">Grecia</option>
    <option value="770">Grenada</option>
    <option value="780">Groenlandia</option>
    <option value="790">Guadalupe</option>
    <option value="800">Guam</option>
    <option value="810">Guatemala</option>
    <option value="820">Guayana Francesa</option>
    <option value="830">Guinea</option>
    <option value="840">Guinea Ecuatorial</option>
    <option value="850">Guinea-Bissau</option>
    <option value="860">Guyana</option>
    <option value="870">Haití</option>
    <option value="880">Holanda</option>
    <option value="890">Honduras</option>
    <option value="900">Hong Kong</option>
    <option value="910">Hungría</option>
    <option value="920">India</option>
    <option value="930">Indonesia</option>
    <option value="940">Irak</option>
    <option value="950">Irán</option>
    <option value="960">Irlanda</option>
    <option value="970">Islandia</option>
    <option value="980">Islas Cayman</option>
    <option value="990">Islas Cook</option>
    <option value="1000">Islas Faroe</option>
    <option value="1010">Islas Marianas del Norte</option>
    <option value="1020">Islas Marshall</option>
    <option value="1030">Islas Solomon</option>
    <option value="1040">Islas Turcas y Caicos</option>
    <option value="1050">Islas Vírgenes</option>
    <option value="1060">Islas Wallis y Futuna</option>
    <option value="1070">Israel</option>
    <option value="1080">Italia</option>
    <option value="1090">Jamaica</option>
    <option value="1100">Japón</option>
    <option value="1110">Jordania</option>
    <option value="1120">Kazajstán</option>
    <option value="1130">Kenya</option>
    <option value="1140">Kirguistán</option>
    <option value="1150">Kiribati</option>
    <option value="1160">Kuwait</option>
    <option value="1170">Laos</option>
    <option value="1180">Lesotho</option>
    <option value="1190">Letonia</option>
    <option value="1200">Líbano</option>
    <option value="1210">Liberia</option>
    <option value="1220">Libia</option>
    <option value="1230">Liechtenstein</option>
    <option value="1240">Lituania</option>
    <option value="1250">Luxemburgo</option>
    <option value="1260">Macao</option>
    <option value="1270">Macedonia</option>
    <option value="1280">Madagascar</option>
    <option value="1290">Malasia</option>
    <option value="1300">Malawi</option>
    <option value="1310">Mali</option>
    <option value="1320">Malta</option>
    <option value="1330">Marruecos</option>
    <option value="1340">Martinica</option>
    <option value="1350">Mauricio</option>
    <option value="1360">Mauritania</option>
    <option value="1370">Mayotte</option>
    <option value="1380">México</option>
    <option value="1390">Micronesia</option>
    <option value="1400">Moldova</option>
    <option value="1410">Mónaco</option>
    <option value="1420">Mongolia</option>
    <option value="1430">Montserrat</option>
    <option value="1440">Mozambique</option>
    <option value="1450">Myanmar</option>
    <option value="1460">Namibia</option>
    <option value="1470">Nauru</option>
    <option value="1480">Nepal</option>
    <option value="1490">Nicaragua</option>
    <option value="1500">Níger</option>
    <option value="1510">Nigeria</option>
    <option value="1520">Noruega</option>
    <option value="1530">Nueva Caledonia</option>
    <option value="1540">Nueva Zelandia</option>
    <option value="1550">Omán</option>
    <option value="1570">Pakistán</option>
    <option value="1580">Panamá</option>
    <option value="1590">Papua Nueva Guinea</option>
    <option value="paraguay">Paraguay</option>
    <option value="1610">Perú</option>
    <option value="1620">Pitcairn</option>
    <option value="1630">Polinesia Francesa</option>
    <option value="1640">Polonia</option>
    <option value="1650">Portugal</option>
    <option value="1660">Puerto Rico</option>
    <option value="1670">Qatar</option>
    <option value="1680">RD Congo</option>
    <option value="1690">Reino Unido</option>
    <option value="1700">República Centroafricana</option>
    <option value="1710">República Checa</option>
    <option value="1720">República Dominicana</option>
    <option value="1730">Reunión</option>
    <option value="1740">Rumania</option>
    <option value="1750">Rusia</option>
    <option value="1760">Rwanda</option>
    <option value="1770">Sahara Occidental</option>
    <option value="1780">Saint Pierre y Miquelon</option>
    <option value="1790">Samoa</option>
    <option value="1800">Samoa Americana</option>
    <option value="1810">San Cristóbal y Nevis</option>
    <option value="1820">San Marino</option>
    <option value="1830">Santa Elena</option>
    <option value="1840">Santa Lucía</option>
    <option value="1850">Sao Tomé y Príncipe</option>
    <option value="1860">Senegal</option>
    <option value="1870">Serbia y Montenegro</option>
    <option value="1880">Seychelles</option>
    <option value="1890">Sierra Leona</option>
    <option value="1900">Singapur</option>
    <option value="1910">Siria</option>
    <option value="1920">Somalia</option>
    <option value="1930">Sri Lanka</option>
    <option value="1940">Sudáfrica</option>
    <option value="1950">Sudán</option>
    <option value="1960">Suecia</option>
    <option value="1970">Suiza</option>
    <option value="1980">Suriname</option>
    <option value="1990">Swazilandia</option>
    <option value="2000">Taiwán</option>
    <option value="uruguay">Uruguay</option>
</select>
</td>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">TipoDocumento</label>
					<div class="col-sm-3">
						<input type="text" name="TipoDocumentoT" class="form-control" placeholder="TipoDocumento" required>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Numerodocumento</label>
					<div class="col-sm-3">
						<input type="text" name="NumerodocumentoT" class="form-control" placeholder="Numerodocumento" required>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Celular</label>
					<div class="col-sm-3">
						<input type="text" name="CelularT" class="form-control" placeholder="Celular" required>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Acepto ser tutor de menor de edad</label>
					<div class="col-sm-3">
						<select name="AceptoT">
						 <option>NO</option>
						 <option>SI</option>
						</select>
					</div>
				</div>
				
				<br>
				
				
				
				<?php echo '<a href="https://mpago.la/1J2uA19"  target="_blank">Pago mensual $500</a>';?>
				<br>
				<br>
	            <?php echo '<a href="https://mpago.la/2AJzpCB"  target="_blank">Pago anual $3000</a>';?>
				
				<br>
				<br>
				<br>
				
				<label class="col-sm-3 control-label">Para autorizar el video, luego del pago enviar captura de recibo o archivo adjunto al siguiente mail: no-reply@c1930051.ferozo.com</label>
				<br>
				<br>
				<br>
				
		<h2>Video de jugadas &raquo; </h2>
		  <div class="form-group">
		
				<input type="file"  name="fileToUpload" id="fileToUpload" onchange="upload_image();">
			<p class="help-block">Seleccione un video</p>
		  </div>
		  <div class="upload-msg"></div><!--Para mostrar la respuesta del archivo llamado via ajax -->
		
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script>

    
	function upload_image(){//Funcion encargada de enviar el archivo via AJAX
				$(".upload-msg").text('Cargando...');
				var inputFileImage = document.getElementById("fileToUpload");
				var file = inputFileImage.files[0];
				var data = new FormData();
				
				data.append('fileToUpload',file);
			    
				
				
				
				
				
				/*jQuery.each($('#fileToUpload')[0].files, function(i, file) {
					data.append('file'+i, file);
				});*/
							
				$.ajax({
					url: "upload.php",        // Url to which the request is send
					type: "POST",             // Type of request to be send, called as method
					data: data, 			  // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false,       // The content type used when sending data to the server.
					cache: false,             // To unable request pages to be cached
					processData:false,        // To send DOMDocument or non processed data file it is set to false
					success: function(data)   // A function to be called if request succeeds
					{
						$(".upload-msg").html(data);
						window.setTimeout(function() {
						$(".alert-dismissible").fadeTo(500, 0).slideUp(500, function(){
						$(this).remove();
						});	}, 5000);
					}
				});
				
			}
</script>		
				
                 
			
				
				<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input type="submit" name="add" class="btn btn-sm btn-primary" value="Guardar datos">
						<a href="http://c1930051.ferozo.com/index.html" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>
   
    <script src="proyectos/dropzone-5.7.0/distdropzone.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
	$('.date').datepicker({
		format: 'dd-mm-yyyy',
	})
	</script>
	
	
	
</body>
</html>