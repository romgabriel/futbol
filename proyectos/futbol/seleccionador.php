<?php
include("conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Club</title>
 
	<!-- Bootstrap -->
	

<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="dropzone.js"></script>
<script src="jquery-3.6.0.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
	
	
	<link href="bootstrap.min.css" rel="stylesheet">
	<link href="bootstrap-datepicker.css" rel="stylesheet">
	<link href="style_nav.css" rel="stylesheet">
	
	<style>
		.content {
			margin-top: 80px;
		}
	</style>
 
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	
	<div class="container">
		<div class="content">
			<h2>Registrar Club &raquo; </h2>
			<hr />
 
			<?php
			if(isset($_POST['add'])){
				 
				$RazonSocial		     = mysqli_real_escape_string($con,(strip_tags($_POST["RazonSocial"],ENT_QUOTES)));//Escanpando caracteres 
				$idUsuario		     = mysqli_real_escape_string($con,(strip_tags($_POST["idUsuario"],ENT_QUOTES)));//Escanpando caracteres
				$Clave	 = mysqli_real_escape_string($con,(strip_tags($_POST["Clave"],ENT_QUOTES)));//Escanpando caracteres 
				$Ciudad	 = mysqli_real_escape_string($con,(strip_tags($_POST["Ciudad"],ENT_QUOTES)));//Escanpando caracteres 
				$Provincia	     = mysqli_real_escape_string($con,(strip_tags($_POST["Provincia"],ENT_QUOTES)));//Escanpando caracteres 
				$Calle		 = mysqli_real_escape_string($con,(strip_tags($_POST["Calle"],ENT_QUOTES)));//Escanpando caracteres 
				$Numerocalle		 = mysqli_real_escape_string($con,(strip_tags($_POST["Numerocalle"],ENT_QUOTES)));//Escanpando caracteres 
				$Piso			 = mysqli_real_escape_string($con,(strip_tags($_POST["Piso"],ENT_QUOTES)));//Escanpando caracteres 
				$departamento		     = mysqli_real_escape_string($con,(strip_tags($_POST["departamento"],ENT_QUOTES)));//Escanpando caracteres 
				$Barrio		     = mysqli_real_escape_string($con,(strip_tags($_POST["Barrio"],ENT_QUOTES)));//Escanpando caracteres 
				$codigoPostal		     = mysqli_real_escape_string($con,(strip_tags($_POST["codigoPostal"],ENT_QUOTES)));//Escanpando caracteres
				$Telefono	 = mysqli_real_escape_string($con,(strip_tags($_POST["Telefono"],ENT_QUOTES)));//Escanpando caracteres 
				$Mail	 = mysqli_real_escape_string($con,(strip_tags($_POST["Mail"],ENT_QUOTES)));//Escanpando caracteres 
				$CUIT	     = mysqli_real_escape_string($con,(strip_tags($_POST["CUIT"],ENT_QUOTES)));//Escanpando caracteres 
				
				
			
				
			
			
			
 
				$cek = mysqli_query($con, "SELECT * FROM jugador WHERE idjugador='1'");
				
				if(mysqli_num_rows($cek) == 0){
						$insert = mysqli_query($con, "INSERT INTO `seleccionador` (`IdSeleccionador`, `RazonSocial`, `Usuario`, `Clave`, `Ciudad`, `Provincia`, `Calle`, `Numerocalle`, `Piso`, `departamento`, `Barrio`, `codigoPostal`, `Telefono`, `Mail`, `CUIT`, `estado`) VALUES
						(NULL, '$RazonSocial', '$idUsuario', '$Clave', '$Ciudad', '$Provincia', '$Calle', '$Numerocalle', '$Piso','#departamento','$Barrio', '$codigoPostal', '$Telefono', '$Mail', '$CUIT', 'Habilitado')") or die(mysqli_error());
						if($insert){
							echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Bien hecho! Los datos han sido guardados con éxito.</div>';
						}else{
							echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. No se pudo guardar los datos !</div>';
						}
					 
				}else{
					echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error. código exite!</div>';
				}
			}
			?>
 
			<form class="form-horizontal" action="" method="post">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Razon Social</label>
					<div class="col-sm-4">
						<input type="text" name="RazonSocial" class="form-control" placeholder="RazonSocial" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">usuario</label>
					<div class="col-sm-4">
						<input type="text" name="idUsuario" class="form-control" placeholder="idUsuario" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Clave</label>
					<div class="col-sm-4">
						<input type="text" name="Clave" class="form-control" placeholder="Clave" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Ciudad</label>
					<div class="col-sm-3">
						<textarea name="Ciudad" class="form-control" placeholder="Ciudad"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Provincia</label>
					<div class="col-sm-3">
					<select name="Provincia">
					     <option value="Buenos Aires">Bs. As.</option>
          <option value="Catamarca">Catamarca</option>
          <option value="Chaco">Chaco</option>
          <option value="Chubut">Chubut</option>
          <option value="Cordoba">Cordoba</option>
          <option value="Corrientes">Corrientes</option>
          <option value="Entre Rios">Entre Rios</option>
          <option value="Formosa">Formosa</option>
          <option value="Jujuy">Jujuy</option>
          <option value="La Pampa">La Pampa</option>
          <option value="La Rioja">La Rioja</option>
          <option value="Mendoza">Mendoza</option>
          <option value="Misiones">Misiones</option>
          <option value="Neuquen">Neuquen</option>
          <option value="Rio Negro">Rio Negro</option>
          <option value="Salta">Salta</option>
          <option value="San Juan">San Juan</option>
          <option value="San Luis">San Luis</option>
          <option value="Santa Cruz">Santa Cruz</option>
          <option value="Santa Fe">Santa Fe</option>
          <option value="Sgo. del Estero">Sgo. del Estero</option>
          <option value="Tierra del Fuego">Tierra del Fuego</option>
         <option value="Tucuman">Tucuman</option>
		 </select>
					
		
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Calle</label>
					<div class="col-sm-3">
						<input type="text" name="Calle" class="form-control" placeholder="Calle" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Numero Calle</label>
					<div class="col-sm-3">
						<input type="text" name="Numerocalle" class="form-control" placeholder="Numerocalle" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Piso</label>
					<div class="col-sm-3">
						<input type="text" name="Piso" class="form-control" placeholder="Piso" required>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">departamento</label>
					<div class="col-sm-3">
						<input type="text" name="departamento" class="form-control" placeholder="departamento" required>
					</div>
				</div>
				
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Barrio</label>
					<div class="col-sm-3">
						<input type="text" name="Barrio" class="form-control" placeholder="Barrio" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Codigo Postal</label>
					<div class="col-sm-3">
						<input type="text" name="codigoPostal" class="form-control" placeholder="codigoPostal" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Telefono</label>
					<div class="col-sm-3">
						<input type="text" name="Telefono" class="form-control" placeholder="Telefono" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Mail</label>
					<div class="col-sm-3">
						<input type="text" name="Mail" class="form-control" placeholder="Mail" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">CUIT</label>
					<div class="col-sm-3">
						<input type="text" name="CUIT" class="form-control" placeholder="CUIT" required>
					</div>
				</div>
			
		
				
                 
			
				
				<div class="form-group">
					<label class="col-sm-3 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<input type="submit" name="add" class="btn btn-sm btn-primary" value="Guardar datos">
						
						<a href="http://c1930051.ferozo.com/index.html" class="btn btn-sm btn-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>
   
    <script src="proyectos/dropzone-5.7.0/distdropzone.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
	$('.date').datepicker({
		format: 'dd-mm-yyyy',
	})
	</script>
	
	
	
</body>
</html>